<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->integer('category_id');
            $table->string('title',75);
            $table->string('subtitle',50);
            $table->string('slug',100)->unique();
            $table->integer('qty');
            $table->double('price',10,2);
            $table->json('product_characteristics');
            $table->string('short_description', 255);
            $table->text('description');
            $table->string('image',255);
            $table->string('gallery',1000);
            $table->string('meta_title', 255)->nullable();
            $table->string('meta_description', 500)->nullable();
            $table->integer('order')->default(1)->nullable();
            $table->boolean('status')->default(0)->nullable();
            $table->boolean('in_stock')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
