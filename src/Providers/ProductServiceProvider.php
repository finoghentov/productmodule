<?php
namespace RomarkCode\Product\Providers;

use Illuminate\Support\ServiceProvider;


class ProductServiceProvider extends ServiceProvider
{
    public function register()
    {

    }

    public function boot(){
        $this->loadMigrationsFrom(__DIR__ . '../database/migrations');
    }

}
