<?php
/**
 * Contains the Buyable interface.
 *
 * @copyright   Copyright (c) 2017 Attila Fulop
 * @author      Attila Fulop
 * @license     MIT
 * @since       2017-10-28
 *
 */

namespace RomarkCode\Product\Contracts;

use Carbon\Carbon;

interface Buyable
{
    /**
     * Returns the id of the _thing_
     *
     * @return int
     */
    public function getId();

    /**
     * Returns the name of the _thing_
     *
     * @return string
     */
    public function getName();

    /**
     * Returns the price of the item; float is temporary!!
     *
     * @return float
     */
    public function getPrice();

    /**
     * Returns the URL of the item's (main) image, or null if there's no image
     *
     * @return string|null
     */
    public function getImageUrl();

    public function addSale(Carbon $date, $units = 1);

    public function removeSale($units = 1);
}
