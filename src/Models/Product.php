<?php

namespace RomarkCode\Product\Models;

use Illuminate\Database\Eloquent\Model;
use RomarkCode\Product\Contracts\Buyable;
use RomarkCode\Product\Traits\BuyableTrait;

class Product extends Model implements Buyable
{
    use BuyableTrait;

    protected $guarded = [];

    protected $attributes = [
        'status' => 0,
        'order' => 1,
        'in_stock' => 0,
    ];

    public function category(){
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function testimonials(){
        return $this->hasMany(Testimonial::class);
    }
}
